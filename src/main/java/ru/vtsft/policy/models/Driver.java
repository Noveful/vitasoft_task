package ru.vtsft.policy.models;

import com.google.gwt.user.client.rpc.IsSerializable;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "drivers", catalog = "vtsft_db")
public class Driver implements Serializable, IsSerializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Date birthDate;
    private int sex;
    private Long category;
    private boolean accepted;

    public Driver(){

    }

    public Driver(DriverDTO driverDTO){
        this.setId(driverDTO.getId());
        this.setName(driverDTO.getName());
        this.setBirthDate(driverDTO.getBirthDate());
        this.setSex(driverDTO.getSex());
        this.setCategory(driverDTO.getCategory());
        this.setAccepted(driverDTO.isAccepted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

}
