package ru.vtsft.policy.gwt.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.hibernate.Criteria;
import org.hibernate.Session;
import ru.vtsft.policy.gwt.client.InsurancePolicyRemoteService;
import ru.vtsft.policy.gwt.client.dto.DriverCategoryDTO;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;
import ru.vtsft.policy.models.Driver;
import ru.vtsft.policy.models.DriverCategory;
import ru.vtsft.policy.util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class InsurancePolicyRemoteServiceImpl extends RemoteServiceServlet implements InsurancePolicyRemoteService {

    @Override
    public List<DriverDTO> getDrivers(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Driver.class);
        List<Driver> driverList = criteria.list();

        List<DriverDTO> driverDtoList = new ArrayList<DriverDTO>();
        for (Driver driver : driverList){
            DriverDTO driverDTO = new DriverDTO();
            driverDTO.setId(driver.getId());
            driverDTO.setName(driver.getName());
            driverDTO.setBirthDate(driver.getBirthDate());
            driverDTO.setSex(driver.getSex());
            driverDTO.setCategory(driver.getCategory());
            driverDTO.setAccepted(driver.isAccepted());
            driverDtoList.add(driverDTO);
        }

        return driverDtoList;
    }

    @Override
    public List<DriverCategoryDTO> getDriverCategories(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Criteria criteria = session.createCriteria(DriverCategory.class);
        List<DriverCategory> driverCategoryList = criteria.list();

        List<DriverCategoryDTO> driverCategoryDtoList = new ArrayList<DriverCategoryDTO>();
        for (DriverCategory driverCategory : driverCategoryList){
            DriverCategoryDTO driverCategoryDto = new DriverCategoryDTO();
            driverCategoryDto.setId(driverCategory.getId());
            driverCategoryDto.setTitle(driverCategory.getTitle());
            driverCategoryDtoList.add(driverCategoryDto);
        }

        return driverCategoryDtoList;
    }

    @Override
    public Long saveDriver(DriverDTO driverDTO){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Driver driver = new Driver(driverDTO);

        session.saveOrUpdate(driver);
        session.getTransaction().commit();
        session.close();

        return driver.getId();
    }

    @Override
    public void deleteDriver(DriverDTO driverDTO){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Driver driver = new Driver(driverDTO);

        session.delete(driver);
        session.getTransaction().commit();
        session.close();
    }

}