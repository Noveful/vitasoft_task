package ru.vtsft.policy.gwt.client.validators;

public class IsEmptyValidator extends Validator {

    @Override
    public boolean validate(String value) {
        if ("".equals(value)){
            errorMessage = "Поле не должно быть пустым";
            return false;
        }
        errorMessage = "";
        return true;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
