package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.ui.Button;

public class OperationButton extends Button{

    public OperationButton(String title){
        super(title);
        setStyleName("custom_button");
    }

}
