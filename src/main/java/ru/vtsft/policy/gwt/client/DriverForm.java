package ru.vtsft.policy.gwt.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import ru.vtsft.policy.gwt.client.dto.DriverCategoryDTO;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;
import ru.vtsft.policy.gwt.client.elements.*;
import ru.vtsft.policy.gwt.client.util.DateUtil;
import ru.vtsft.policy.gwt.client.validators.IsEmptyValidator;

import java.util.*;

public class DriverForm extends VerticalPanel implements Observable {

    private FormField nameFld = new FormField("ФИО");
    private DateField birthdateFld = new DateField("Дата рождения");
    private FormField ageFld = new FormField("Возраст");
    private RadioButtonGroup sexFld = new RadioButtonGroup("sex");
    private FormListBox categoryFld = new FormListBox();
    private OperationButton saveBtn = new OperationButton("Сохранить");
    private OperationButton clearBtn = new OperationButton("Очистить форму");

    private Map<Long, DriverCategoryDTO> driverCategories;
    private DriverDTO editingDriverDto;

    private List<Observer> observers = new ArrayList<Observer>();
    private Set<Validatable> validateFields = new HashSet<Validatable>();

    public DriverForm(){
        buildGui();

        validateFields.add(nameFld);
        validateFields.add(birthdateFld);
//        validateFields.add(sexFld);
    }

    public void setData(DriverDTO driver){
        editingDriverDto = driver;

        nameFld.setValue(driver.getName());
        birthdateFld.setDate(driver.getBirthDate());
        ageFld.setValue(String.valueOf(DateUtil.getAge(driver.getBirthDate())));
        sexFld.setSelectedValue(driver.getSex());
        categoryFld.setSelectedValue(String.valueOf(driver.getCategory()));
    }

    public void setDriverCategories(Map<Long, DriverCategoryDTO> driverCategories){
        this.driverCategories = driverCategories;
        for (DriverCategoryDTO driverCategory : driverCategories.values()){
            categoryFld.addItem(driverCategory.getTitle(), String.valueOf(driverCategory.getId()));
        }
    }

    public void clearFields(){
        editingDriverDto = null;

        nameFld.setValue("");
        birthdateFld.clearValue();
        ageFld.setValue("");
        sexFld.clearValue();
        categoryFld.clearValue();
    }

    private void buildGui(){
        FormLabel sexTitle = new FormLabel("Пол");
        FormLabel categoryTitle = new FormLabel("Категория");
        sexFld.addRadioButton(1, "М");
        sexFld.addRadioButton(2, "Ж");

        ageFld.setEnabled(false);
        categoryFld.addItem("Выберите категорию...", "0");

        add(nameFld);
        add(birthdateFld);
        add(ageFld);
        add(sexTitle);
        add(sexFld);
        add(categoryTitle);
        add(categoryFld);
        add(new InlineHTML("</br>"));
        HorizontalPanel buttonPanel = new HorizontalPanel();
        buttonPanel.add(clearBtn);
        buttonPanel.add(saveBtn);
        add(buttonPanel);

        nameFld.addValidator(new IsEmptyValidator());
        birthdateFld.addValidator(new IsEmptyValidator());

        addHandlers();
    }

    private void addHandlers(){
        birthdateFld.addChangedHandler(new ValueChangeHandler() {
            @Override
            public void onValueChange(ValueChangeEvent event) {
                ageFld.setValue(String.valueOf(DateUtil.getAge(birthdateFld.getDate())));
            }
        });

        saveBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                save();
            }
        });

        clearBtn.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clearFields();
            }
        });
    }

    private boolean validate(){
        boolean isValidate = true;
        for (Validatable field : validateFields){
            if (field.validate()) continue;
            isValidate = false;
        }

        boolean sexValidate = sexFld.validate();
        if (!sexValidate){
            isValidate = sexValidate;
        }

        boolean categoryValidate = categoryFld.validate();
        if (!categoryValidate){
            isValidate = categoryValidate;
        }

        return isValidate;
    }

    private void save(){
        if (!validate()) return;

        if (editingDriverDto == null) editingDriverDto = new DriverDTO();
        editingDriverDto.setName(nameFld.getValue());
        editingDriverDto.setBirthDate(birthdateFld.getDate());
        editingDriverDto.setSex(sexFld.getSelectedValue());
        editingDriverDto.setCategory(new Long(categoryFld.getSelectedValue()));
        editingDriverDto.setAccepted(true);

        InsurancePolicyRemoteService.App.getInstance().saveDriver(editingDriverDto, new AsyncCallback<Long>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Данные сохранить не удалось");
            }

            @Override
            public void onSuccess(Long result) {
                Window.alert("Сохранение прошло успешно");
                editingDriverDto.setId(result);
                notifyObservers();
                editingDriverDto = null;
                clearFields();
            }
        });
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer :observers){
            observer.update(editingDriverDto);
        }
        editingDriverDto = null;
    }
}
