package ru.vtsft.policy.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.vtsft.policy.gwt.client.dto.DriverCategoryDTO;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;
import ru.vtsft.policy.models.Driver;

import java.util.List;

public interface InsurancePolicyRemoteServiceAsync {
    void getDrivers(AsyncCallback<List<DriverDTO>> async);

    void getDriverCategories(AsyncCallback<List<DriverCategoryDTO>> async);

    void saveDriver(DriverDTO driverDTO, AsyncCallback<Long> async);

    void deleteDriver(DriverDTO driverDTO, AsyncCallback<Void> async);
}
