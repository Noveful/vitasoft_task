package ru.vtsft.policy.gwt.client;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.query.client.GQuery;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import ru.vtsft.policy.gwt.client.dto.DriverCategoryDTO;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;
import ru.vtsft.policy.gwt.client.elements.*;
import ru.vtsft.policy.gwt.client.enums.Sex;
import ru.vtsft.policy.gwt.client.util.DateUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DriversTable extends VerticalPanel implements Observer{
    public static final int NAME_WIDTH = 300;
    public static final int BIRTHDATE_WIDTH = 150;
    public static final int AGE_WIDTH = 100;
    public static final int SEX_WIDTH = 50;
    public static final int CATEGORY_WIDTH = 120;
    public static final int EDIT_WIDTH = 100;
    public static final int DELETE_WIDTH = 70;
    private static final DateTimeFormat dtf = DateTimeFormat.getFormat("dd/MM/yyyy");

    private VerticalPanel driverListTable = new VerticalPanel();
    private VerticalPanel searchPanel = new VerticalPanel();
    private VerticalPanel formPanel = new VerticalPanel();

    private SuggestBox searchField;
    private Label emptySearchResultLbl = new Label("");
    private DriverForm driverForm = new DriverForm();

    private List<DriverDTO> drivers;
    private Map<Long, DriverDTO> acceptedDrivers;
    private Map<Long, DriverDTO> nonacceptedDrivers;
    private Map<String, Long> searchDataBase;
    private Map<Long, DriverCategoryDTO> driverCategories = new HashMap<Long, DriverCategoryDTO>();
    private Map<Long, DriverLine> driverLines = new HashMap<Long, DriverLine>();

    public DriversTable(){
        loadData();

        Label tableTitle = new Label("Таблица водителей, допущенных к управлению транспортом");
        tableTitle.setStyleName("drivers_table_title");
        add(tableTitle);
        add(driverListTable);

        add(new InlineHTML("<br/>"));

        Label searchFieldTitle = new Label("Форма поиска");
        searchFieldTitle.setStyleName("search_field_title");
        emptySearchResultLbl.setStyleName("empty_search_result");
        emptySearchResultLbl.setHeight("10px");
        add(searchFieldTitle);
        add(searchPanel);
        add(emptySearchResultLbl);

        add(new InlineHTML("<br/>"));

        Label driverFormTitle = new Label("Форма редактирования данных водителя");
        driverFormTitle.setStyleName("driver_form_title");
        driverFormTitle.setWidth("1000px");
        add(driverFormTitle);
        add(formPanel);

        buildHeader();
        formPanel.add(driverForm);

        driverForm.registerObserver(DriversTable.this);
    }

    private void loadData() {
        InsurancePolicyRemoteService.App.getInstance().getDriverCategories(new AsyncCallback<List<DriverCategoryDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Что-то пошло не так. Не удалось получить список категорий водительских прав.");
            }

            @Override
            public void onSuccess(List<DriverCategoryDTO> result) {
                if (result == null) return;
                for (DriverCategoryDTO driverCategory : result) {
                    driverCategories.put(driverCategory.getId(), driverCategory);
                }

                loadDrivers();
            }
        });
    }

    private void loadDrivers() {
        InsurancePolicyRemoteService.App.getInstance().getDrivers(new AsyncCallback<List<DriverDTO>>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert("Что-то пошло не так. Не удалось получить список водителей.");
            }

            @Override
            public void onSuccess(List<DriverDTO> result) {
                drivers = result;
                if (acceptedDrivers == null) acceptedDrivers = new HashMap<Long, DriverDTO>();
                if (nonacceptedDrivers == null) nonacceptedDrivers = new HashMap<Long, DriverDTO>();
                if (searchDataBase == null) searchDataBase = new HashMap<String, Long>();

                for (DriverDTO driverDTO : drivers) {
                    if (driverDTO.isAccepted()) {
                        acceptedDrivers.put(driverDTO.getId(), driverDTO);
                        continue;
                    }

                    nonacceptedDrivers.put(driverDTO.getId(), driverDTO);
                    searchDataBase.put(driverDTO.getName() + " " + dtf.format(driverDTO.getBirthDate()),
                            driverDTO.getId());
                }

                for (DriverDTO driver : acceptedDrivers.values()) {
                    DriverLine driverLine = new DriverLine(driver);
                    DriversTable.this.driverListTable.add(driverLine);
                    driverLines.put(driver.getId(), driverLine);
                }

                buildSearchPanel();

                driverForm.setDriverCategories(driverCategories);
            }
        });
    }

    private void buildHeader(){
        HorizontalPanel headerPanel = new HorizontalPanel();
        headerPanel.setStyleName("data_table_title_panel");

        CellLabel nameLbl = new CellLabel("ФИО", NAME_WIDTH, "table_head");
        CellLabel birthdayLbl = new CellLabel("Дата рождения", BIRTHDATE_WIDTH, "table_head");
        CellLabel ageLbl = new CellLabel("Возраст", AGE_WIDTH, "table_head");
        CellLabel sexLbl = new CellLabel("Пол", SEX_WIDTH, "table_head");
        CellLabel categoryLbl = new CellLabel("Категория", CATEGORY_WIDTH, "table_head");
        CellLabel editLbl = new CellLabel("", EDIT_WIDTH, "table_head");
        CellLabel deleteLbl = new CellLabel("", DELETE_WIDTH, "table_head");

        headerPanel.add(nameLbl);
        headerPanel.add(birthdayLbl);
        headerPanel.add(ageLbl);
        headerPanel.add(sexLbl);
        headerPanel.add(categoryLbl);
        headerPanel.add(editLbl);
        headerPanel.add(deleteLbl);

        driverListTable.add(headerPanel);
    }

    private void buildSearchPanel(){
        MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();
        for (String str : searchDataBase.keySet()) {
            oracle.add(str);
        }
        searchField = new SuggestBox(oracle);
        searchField.setWidth("300px");
        searchField.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
            @Override
            public void onSelection(SelectionEvent<SuggestOracle.Suggestion> event) {
                onSelectDriver();
            }
        });

        searchField.addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                Timer timer = new Timer() {
                    @Override
                    public void run() {
                        if (searchField.getValue() != "" && GQuery.$(".gwt-SuggestBoxPopup").length() != 1){
                            emptySearchResultLbl.setText("Поиск дал пустой результат");
                            return;
                        }
                        emptySearchResultLbl.setText("");
                    }
                };
                timer.schedule(500);

            }
        });
        searchPanel.add(searchField);
    }

    private void onSelectDriver(){
        Long selectedDriverId = searchDataBase.get(searchField.getValue());
        if (selectedDriverId != null) {
            DriverDTO selectedDriver = nonacceptedDrivers.get(selectedDriverId);
            DriverLine newLine = new DriverLine(selectedDriver);
            driverLines.put(selectedDriverId, newLine);
            driverListTable.add(newLine);

            acceptedDrivers.put(selectedDriverId, selectedDriver);
            nonacceptedDrivers.remove(selectedDriverId);

            searchDataBase.remove(searchField.getValue());
            updateSearchItems();

            selectedDriver.setAccepted(true);
            InsurancePolicyRemoteService.App.getInstance().saveDriver(selectedDriver, new AsyncCallback<Long>() {
                @Override
                public void onFailure(Throwable caught) {
                    Window.alert("Не удалось допустить водителя к управлению транспортом");
                }

                @Override
                public void onSuccess(Long result) {

                }
            });
        }
        searchField.setValue("");
    }

    private void updateSearchItems(){
        MultiWordSuggestOracle oracle = (MultiWordSuggestOracle)searchField.getSuggestOracle();
        oracle.clear();
        oracle.addAll(searchDataBase.keySet());
    }

    @Override
    public void update(Object obj) {
        try {
            DriverDTO editedDriverDto = (DriverDTO) obj;

            if (driverLines.containsKey(editedDriverDto.getId())){
                DriverLine line = driverLines.get(editedDriverDto.getId());
                line.updateData(editedDriverDto);
                return;
            }

            DriverLine line = new DriverLine(editedDriverDto);
            driverListTable.add(line);
            driverLines.put(editedDriverDto.getId(), line);

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private class DriverLine extends HorizontalPanel{
        private DriverDTO driver;
        private CellLabel nameCell;
        private CellLabel birthdayCell;
        private CellLabel ageCell;
        private CellLabel sexCell;
        private CellLabel categoryCell;
        private OperationButton edit;
        private OperationButton delete;

        private EventHandler eventHandler = new EventHandler();

        public DriverLine(DriverDTO driver){
            this.driver = driver;
            buildGui();
        }

        public void updateData(DriverDTO driverDTO){
            nameCell.setText(driverDTO.getName());
            birthdayCell.setText(dtf.format(driverDTO.getBirthDate()));
            ageCell.setText(String.valueOf(DateUtil.getAge(driverDTO.getBirthDate())));
            sexCell.setText(Sex.get(driverDTO.getSex()).getValue());
            categoryCell.setText(driverCategories.get(driverDTO.getCategory()).getTitle());
        }

        private void buildGui(){
            nameCell = new CellLabel(driver.getName(), NAME_WIDTH);
            birthdayCell = new CellLabel(dtf.format(driver.getBirthDate()), BIRTHDATE_WIDTH);
            ageCell = new CellLabel(String.valueOf(DateUtil.getAge(driver.getBirthDate())), AGE_WIDTH);
            Sex sex = Sex.get(driver.getSex());
            String sexTitle = (sex!=null)?sex.getValue():"";
            sexCell = new CellLabel(sexTitle, SEX_WIDTH);
            categoryCell = new CellLabel(driverCategories.get(driver.getCategory()).getTitle(), CATEGORY_WIDTH);
            edit = new OperationButton("Редактировать");
            delete = new OperationButton("Удалить");

            add(nameCell);
            add(birthdayCell);
            add(ageCell);
            add(sexCell);
            add(categoryCell);
            add(edit);
            add(delete);

            setStyleName("data_table_row");

            edit.addClickHandler(eventHandler);
            delete.addClickHandler(eventHandler);
        }

        class EventHandler implements ClickHandler {

            @Override
            public void onClick(ClickEvent event) {
                if (event.getSource().equals(edit)){
                    driverForm.setData(driver);
                    return;
                }

                if (event.getSource().equals(delete)){
                    if (!Window.confirm("Вы уверены, что хотите удалить водителя?")) return;
                    driver.setAccepted(false);
                    InsurancePolicyRemoteService.App.getInstance().saveDriver(driver, new AsyncCallback<Long>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            Window.alert("Не удалось удалить водителя");
                        }

                        @Override
                        public void onSuccess(Long result) {
                            Window.alert("Водитель успешно удален");
                            driverLines.get(driver.getId()).removeFromParent();
                            driverLines.remove(driver.getId());

                            nonacceptedDrivers.put(driver.getId(), driver);
                            acceptedDrivers.remove(driver.getId());

                            searchDataBase.put(driver.getName() + " " + dtf.format(driver.getBirthDate()), driver.getId());
                            updateSearchItems();
                        }
                    });
                }
            }
        }
    }
}
