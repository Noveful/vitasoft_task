package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.ui.Label;

public class FormLabel extends Label {
    public FormLabel(String text){
        super(text);
        setStyleName("form_field_label");
    }
}
