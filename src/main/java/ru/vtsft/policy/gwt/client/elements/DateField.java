package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import ru.vtsft.policy.gwt.client.validators.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateField extends VerticalPanel implements Validatable {
    private static String FORM_INPUT_INVALID = "form_input_invalid";

    private FormLabel label;
    private DateBox dateField;
    private ErrorLabel errorLbl;
    private DatePicker datePicker;

    private List<Validator> validators = new ArrayList<Validator>();
    private String errorMessage;

    public DateField(String title){
        label = new FormLabel(title);
        dateField = new DateBox();
        errorLbl = new ErrorLabel();

        DateTimeFormat dateFormat=DateTimeFormat.getFormat("dd MMMM yyyy");
        dateField.setFormat(new DateBox.DefaultFormat(dateFormat));
        datePicker = dateField.getDatePicker();
        styleDatepicker();
        add(label);
        add(dateField);
        add(errorLbl);

        datePicker.setYearAndMonthDropdownVisible(true);
        datePicker.setYearArrowsVisible(true);
        datePicker.setVisibleYearCount(100);
    }

    public Date getDate(){
        return dateField.getValue();
    }

    public void setDate(Date date){
        dateField.setValue(date);
    }

    public void addChangedHandler(ValueChangeHandler handler){
        dateField.addValueChangeHandler(handler);
    }

    public void clearValue(){
        dateField.getTextBox().setValue("");
    }

    private void styleDatepicker(){
        datePicker.setWidth("200px");
        datePicker.setHeight("150px");
    }

    @Override
    public boolean validate() {
        for (Validator validator : validators){
            if (!validator.validate(dateField.getTextBox().getValue())){
                this.errorMessage = validator.getErrorMessage();
                errorLbl.setText(this.errorMessage);
                dateField.getTextBox().setStyleName(FORM_INPUT_INVALID);
                return false;
            }
        }

        this.errorMessage = "";
        errorLbl.setText("");
        dateField.getTextBox().removeStyleName(FORM_INPUT_INVALID);
        return true;
    }

    @Override
    public void addValidator(Validator validator) {
        validators.add(validator);
    }
}
