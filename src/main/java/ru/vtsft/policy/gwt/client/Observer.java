package ru.vtsft.policy.gwt.client;

public interface Observer {
    void update(Object obj);
}
