package ru.vtsft.policy.gwt.client.enums;

public enum Sex {
    MALE(1, "М"),
    FEMALE(2, "Ж");

    private int index;
    private String value;

    private Sex(int index, String value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return this.index;
    }

    public String getValue() {
        return this.value;
    }

    public static Sex get(int index){
        for (Sex sex : Sex.values()){
            if (sex.getIndex() == index) return sex;
        }
        return null;
    }
}