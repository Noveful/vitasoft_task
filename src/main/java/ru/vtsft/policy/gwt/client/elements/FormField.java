package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.ui.FlowPanel;
import ru.vtsft.policy.gwt.client.validators.Validator;

import java.util.ArrayList;
import java.util.List;

public class FormField extends FlowPanel implements Validatable {
    private static String FORM_INPUT_INVALID = "form_input_invalid";

    private String title;
    private FormLabel lbl;
    private FormInput txt;
    private ErrorLabel errorLbl;

    private List<Validator> validators = new ArrayList<Validator>();
    private String errorMessage;

    public FormField(String title){
        this.title = title;
        lbl = new FormLabel(this.title);
        txt = new FormInput();
        errorLbl = new ErrorLabel();

        this.buildGui();
    }

    public String getValue(){
        return txt.getValue();
    }

    public void setValue(String value){
        txt.setValue(value);
    }

    public void clearField(){
        this.setValue("");
        this.txt.removeStyleName(FORM_INPUT_INVALID);
        errorLbl.setText("");
        errorMessage = "";
    }

    public void setEnabled(boolean isEnabled){
        txt.setEnabled(isEnabled);
    }

    private void buildGui(){
        add(lbl);
        add(txt);
        add(errorLbl);
    }

    public void setErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage(){
        return this.errorMessage;
    }

    @Override
    public void addValidator(Validator validator){
        validators.add(validator);
    }

    @Override
    public boolean validate(){
        for (Validator validator : validators){
            if (!validator.validate(this.getValue())){
                this.errorMessage = validator.getErrorMessage();
                errorLbl.setText(this.errorMessage);
                this.txt.setStyleName(FORM_INPUT_INVALID);
                return false;
            }
        }

        this.errorMessage = "";
        errorLbl.setText("");
        this.txt.removeStyleName(FORM_INPUT_INVALID);
        return true;
    }
}