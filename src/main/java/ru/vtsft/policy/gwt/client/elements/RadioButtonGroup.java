package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

import java.util.HashMap;
import java.util.Map;

public class RadioButtonGroup extends VerticalPanel {

    private HorizontalPanel radioPanel = new HorizontalPanel();
    private Map<Integer, RadioButton> radioButtons;
    private ErrorLabel errorLbl = new ErrorLabel();
    private Integer selectedValue;
    private String name;

    public RadioButtonGroup(String name){
        this.name = name;
        add(radioPanel);
        add(errorLbl);
    }

    public Integer getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(Integer selectedValue) {
        this.selectedValue = selectedValue;
        clearValue();
        radioButtons.get(selectedValue).setValue(true);
    }

    public void addRadioButton(final Integer index, String title){
        final RadioButton radioButton = new RadioButton(name, title);
        radioButton.setStyleName("radio_button_item");
        if (radioButtons == null){
            radioButtons = new HashMap<Integer, RadioButton>();
        }
        radioButtons.put(index, radioButton);

        radioPanel.add(radioButton);

        radioButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (radioButton.getValue()){
                    RadioButtonGroup.this.setSelectedValue(index);
                }
            }
        });
    }

    public void clearValue(){
        for (RadioButton radioButton : radioButtons.values()){
            radioButton.setValue(false);
        }
    }

    public boolean validate(){
        if (getSelectedValue() == null) {
            errorLbl.setText("Выберите значение");
            return false;
        }
        errorLbl.setText("");
        return true;
    }

}
