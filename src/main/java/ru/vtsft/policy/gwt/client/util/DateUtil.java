package ru.vtsft.policy.gwt.client.util;

import java.util.Date;

public class DateUtil {
    public static int getAge(Date birthDate){
        Date now = new Date();

        if (now.getYear() > birthDate.getYear()){
            if (now.getMonth() > birthDate.getMonth())
                return now.getYear() - birthDate.getYear();

            if (now.getMonth() < birthDate.getMonth())
                return now.getYear() - birthDate.getYear() - 1;

            if (now.getDate() >= birthDate.getDate())
                return now.getYear() - birthDate.getYear();

            return now.getYear() - birthDate.getYear() - 1;
        }

        return 0;
    }
}
