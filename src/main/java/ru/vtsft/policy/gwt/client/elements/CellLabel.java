package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.ui.InlineHTML;

public class CellLabel extends InlineHTML {
    public CellLabel(String text){
        super(text);
        setWordWrap(true);
        setStyleName("cell_label");
    }

    public CellLabel(String text, int width){
        this(text);
        setWidth(width + "px");
    }

    public CellLabel(String text, int width, String styleName){
        this(text, width);
        addStyleName(styleName);
    }
}
