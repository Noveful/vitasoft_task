package ru.vtsft.policy.gwt.client.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

public class DriverCategoryDTO implements Serializable, IsSerializable {

    private Long id;
    private String title;

    public DriverCategoryDTO(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
