package ru.vtsft.policy.gwt.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;
import ru.vtsft.policy.gwt.client.dto.DriverCategoryDTO;
import ru.vtsft.policy.gwt.client.dto.DriverDTO;
import ru.vtsft.policy.models.Driver;

import java.util.List;

@RemoteServiceRelativePath("InsurancePolicyRemoteService")
public interface InsurancePolicyRemoteService extends RemoteService {
    List<DriverDTO> getDrivers();

    List<DriverCategoryDTO> getDriverCategories();

    Long saveDriver(DriverDTO driverDTO);

    void deleteDriver(DriverDTO driverDTO);

    /**
     * Utility/Convenience class.
     * Use InsurancePolicyRemoteService.App.getInstance() to access static instance of InsurancePolicyRemoteServiceAsync
     */
    public static class App {
        private static final InsurancePolicyRemoteServiceAsync ourInstance = (InsurancePolicyRemoteServiceAsync) GWT.create(InsurancePolicyRemoteService.class);

        public static InsurancePolicyRemoteServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
