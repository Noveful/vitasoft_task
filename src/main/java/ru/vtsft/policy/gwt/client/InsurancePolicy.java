package ru.vtsft.policy.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;

public class InsurancePolicy implements EntryPoint {
    public void onModuleLoad() {
        RootPanel.get("insurance_policy").add(new DriversTable());
    }
}
