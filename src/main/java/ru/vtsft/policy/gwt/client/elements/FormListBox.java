package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import java.util.HashMap;
import java.util.Map;

public class FormListBox extends VerticalPanel {
    private ListBox listBox = new ListBox();
    private ErrorLabel errorLbl = new ErrorLabel();

    private Map<String, Integer> valuesToIndexes = new HashMap<String, Integer>();

    public FormListBox(){
        add(listBox);
        add(errorLbl);
    }

    public void addItem(String title, String value){
        listBox.addItem(title, value);
        valuesToIndexes.put(value, listBox.getItemCount() - 1);
    }

    public void setSelectedValue(String value){
        Integer index = valuesToIndexes.get(value);
        if (index == null) return;
        listBox.setSelectedIndex(index.intValue());
    }

    public String getSelectedValue(){
        return listBox.getSelectedValue();
    }

    public void clearValue(){
        listBox.setSelectedIndex(0);
    }

    public boolean validate(){
        String selectedValue = getSelectedValue();
        if (selectedValue == null || new Integer(selectedValue)==0) {
            errorLbl.setText("Выберите значение");
            return false;
        }
        errorLbl.setText("");
        return true;
    }
}
