package ru.vtsft.policy.gwt.client.elements;

import ru.vtsft.policy.gwt.client.validators.Validator;

public interface Validatable {
    public boolean validate();
    public void addValidator(Validator validator);
}
