package ru.vtsft.policy.gwt.client.validators;

public abstract class Validator {
    public String errorMessage;

    public abstract boolean validate(String value);

    public abstract String getErrorMessage();

}
