package ru.vtsft.policy.gwt.client.elements;

import com.google.gwt.user.client.ui.Label;

public class ErrorLabel extends Label {
    public ErrorLabel(){
        super();
        setStyleName("error_label");
    }
}
